//
//  AppState.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

struct AppState {
    enum Change {
        case add(Add); enum Add {
            case beer(Beer)
            case allBeers([Beer])
            case favorite(Beer)
        }
        case remove(Remove); enum Remove {
            case favorite(Beer)
        }
    }
    
    func alter(by changes:[Change]) -> Self { changes.reduce(self) { $0.alter(by:$1) } }

    init() {
        self.init([],[])
    }
    private init(_ beers: [Beer],_ favorites:[Beer]) {
        self.beers = beers
        self.favorites = favorites
    }
    private func alter(by c:Change) -> Self {
        switch c {
        case let .add(.beer(b))     : Self(beers+[b], favorites    )
        case let .add(.allBeers(b)) : Self(b,         favorites    )
        case let .add(.favorite(b)) : Self(beers,     favorites+[b])
        case let .remove(.favorite(b)): Self(beers, favorites.filter({
            $0.id != b.id
        }))

        }
    }
    
    let beers:[Beer]
    let favorites:[Beer]
}

extension AppState: Codable {}

