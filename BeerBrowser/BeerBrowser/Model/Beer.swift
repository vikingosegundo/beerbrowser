//
//  Beer.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

import Foundation

struct Beer {
    let id:Int
    let name:String
    let tagline:String
    let abv:Double
    let description:String
    let imageURL:URL?
    
    private enum CodingKeys : String, CodingKey {
        case id, name, tagline, abv,description, imageURL = "image_url"
    }
}

extension Beer: Codable {}
extension Beer: Identifiable {}
extension Beer: Hashable {}
