//
//  UseCase.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

protocol UseCase {
    associatedtype RequestType
    associatedtype ResponseType
    func request(to request:RequestType)
}
