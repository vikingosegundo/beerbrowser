//
//  Message.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//


enum Message {
    case beer(_Beer);enum _Beer {
        case load(Load); enum Load {
            case all
        }
        case loaded(Loaded); enum Loaded {
            case all(Outcome)
            enum Outcome {
                case successfully([Beer])
                case failed(Error)
            }
        }
        case favorite(Favorite); enum Favorite {
            case add(Beer)
            case added(Beer)
            case remove(Beer)
            case removed(Beer)

        }
    }
}
