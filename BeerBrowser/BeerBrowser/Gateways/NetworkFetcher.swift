//
//  NetworkFetcher.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

import Foundation

enum NetworkfetchingError:Error {
    case unknown
}

protocol NetworkFetching {
    func loadAllBeers(result: @escaping (Result<[Beer], NetworkfetchingError>) -> ())
}

final class NetworkFetcher:NetworkFetching {
    func loadAllBeers(result: @escaping (Result<[Beer], NetworkfetchingError>) -> ()) {
        load(page: 1, priviouslyLoaded: []) {
            switch $0 {
            case let .success(beers): result(.success(beers))
            case let .failure(error): result(.failure(error))
            }
        }
    }
    
    private func load(page:Int,priviouslyLoaded:[Beer],result: @escaping (Result<[Beer], NetworkfetchingError>) -> ()) {
        let url = URL(string:"https://api.punkapi.com/v2/beers?per_page=80&page=\(page)")!
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: url) { data, response, error in
            guard let data = data else {
                result(.failure(.unknown))
                return
            }
            let beers = beersFrom(data: data)
            guard beers.count > 0 else {
                result(.success(priviouslyLoaded))
                return
            }
            self.load(page: page+1, priviouslyLoaded: priviouslyLoaded+beers,result: result)
        }
        task.resume()
    }
}

fileprivate
func beersFrom(data:Data) -> [Beer] {
    let beers = try! JSONDecoder().decode([Beer].self, from: data)
    return beers
}

