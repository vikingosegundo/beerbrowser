//
//  Store.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

typealias Access<S> = (                    ) -> S  // get current state
typealias Change<C> = ( C...               ) -> () // change state by aplying Change C{1.*}
typealias Reset     = (                    ) -> () // reset to defaults
typealias Updated   = ( @escaping () -> () ) -> () // subscriber
typealias Destroy   = (                    ) -> () // destroy persistent data

typealias Store<S,C> = ( /*S:State, C:Change*/
      state: Access<S>,
     change: Change<C>,
      reset: Reset,
    updated: Updated,
    destroy: Destroy
)

func destroy<S,C>(_  store:inout Store<S,C>! ){ store.destroy();store = nil }
