//
//  createBeerFeature.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 11.11.23.
//

func createBeerFeature(
    store s:AppStore,
    networkFetcher n:NetworkFetching, 
    output o: @escaping Output
) -> Input {
    
    let allBeerLoader    = AllBeerLoader(networkFetcher:n, store:s, responder:process(on:o))
    let favoritesManager = FavoriteManager(                store:s, responder:process(on:o))
    
    func execute(cmd:Message._Beer.Load) {
        switch cmd {
        case  .all: allBeerLoader.request(to: .loadAllBeer)
        }
    }
    
    func execute(cmd:Message._Beer.Favorite) {
        switch cmd {
        case let .add(beer):favoritesManager.request(to: .add(beer))
        case     .added(_) : ()
        case let .remove(beer): favoritesManager.request(to: .remove(beer))
        case     .removed(_): ()
        }
    }
    return {
        if case let .beer(.load    (cmd)) = $0 { execute(cmd:cmd) }
        if case let .beer(.favorite(cmd)) = $0 { execute(cmd:cmd) }
    }
}

fileprivate
func process(on out:@escaping Output) -> (AllBeerLoader.Response) -> () {
    {
        switch $0 {
        case let .allBeerLoaded   (beers): out(.beer(.loaded(.all(.successfully(beers)))))
        case let .loadingAllBeerFailed(e): out(.beer(.loaded(.all(.failed(e)))))
        }
    }
}

fileprivate
func process(on out:@escaping Output) -> (FavoriteManager.Response) -> () {
    {
        switch $0 {
        case let .added  (beer): out(.beer(.favorite(.added(beer))))
        case let .removed(beer): out(.beer(.favorite(.removed(beer))))
        }
    }
}
