//
//  AllBeerLoader.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 12.11.23.
//

struct AllBeerLoader:UseCase {
    enum Request {
        case loadAllBeer
    }
    enum Response {
        case allBeerLoaded([Beer])
        case loadingAllBeerFailed(Error)
    }
    
    init(networkFetcher n:NetworkFetching,store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        networkFetcher = n
        store = s
        respond = r
    }
    
    func request(to request: Request) {
        switch request {
        case .loadAllBeer: networkFetcher.loadAllBeers {
            switch $0 {
            case let .success(beers):  store.change(.add(.allBeers(beers))); respond(.allBeerLoaded(beers))
            case let .failure(e): respond(.loadingAllBeerFailed(e))
            }
        }
        }
    }
    private let networkFetcher:NetworkFetching
    private let store: Store<AppState,AppState.Change>
    private let respond: (Response) -> ()
    
    typealias RequestType = Request
    typealias ResponseType = Response
}
