//
//  FavoriteManager.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 12.11.23.
//

struct FavoriteManager:UseCase {
    enum Request {
        case add(Beer)
        case remove(Beer)
    }
    enum Response {
        case added(Beer)
        case removed(Beer)
    }
    
    init(store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        store = s
        respond = r
    }
    
    func request(to request: Request) {
        switch request {
        case let .add   (beer): store.change(.add   (.favorite(beer))); respond(.added  (beer))
        case let .remove(beer): store.change(.remove(.favorite(beer))); respond(.removed(beer))
        }
    }
    
    private let store: Store<AppState,AppState.Change>
    private let respond: (Response) -> ()
    
    typealias RequestType = Request
    typealias ResponseType = Response
}
