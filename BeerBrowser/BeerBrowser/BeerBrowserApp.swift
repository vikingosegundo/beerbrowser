//
//  BeerBrowserApp.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

import SwiftUI

@main
struct BeerBrowserApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewState: viewState)
                .onAppear{
                    viewState.roothandler(.beer(.load(.all)))
                }
        }
    }
}

fileprivate let store       = createDiskStore()
fileprivate let fetcher     = NetworkFetcher()
fileprivate let roothandler = createAppDomain(store:store, fetcher:fetcher, receivers:[], rootHandler:{ roothandler($0) })
fileprivate let viewState   = ViewState      (store:store, roothandler:roothandler)
