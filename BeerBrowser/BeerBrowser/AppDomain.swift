//
//  AppDomain.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

import Foundation

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()
typealias AppStore = Store<AppState,AppState.Change>

func createAppDomain(
    store      : AppStore,
    fetcher    : NetworkFetching,
    receivers  : [Input],
    rootHandler: @escaping Output
) -> Input
{
    let features: [Input] = [
        createBeerFeature(store:store, networkFetcher:fetcher, output:rootHandler)
    ]
    
    return { msg in
        (receivers + features).forEach {
            $0(msg)
        }
    }
}
