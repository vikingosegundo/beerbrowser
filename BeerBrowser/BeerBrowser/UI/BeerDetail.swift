//
//  BeerDetail.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 12.11.23.
//

import SwiftUI

struct BeerDetail:View {
    var beer:Beer

    var body: some View {
        VStack {
            ScrollView {
                HStack {
                    BeerName(beer:beer).font(.largeTitle)
                    BeerFavorite(beer: beer)
                }.padding()
                BeerTagline(beer:beer).padding().font(.headline)
                BeerDescription(beer:beer)
                BeerABV(beer:beer).font(.title)
                AsyncImage(url: beer.imageURL) {
                    $0
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 200, maxHeight: 300)
                } placeholder: {
                    Image("beerbottle")
                }
            }.padding()
        }
    }
}

