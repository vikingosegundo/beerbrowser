//
//  BeerDescription.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 12.11.23.
//

import SwiftUI

struct BeerDescription:View {
    var beer:Beer
    
    var body: some View {
        Text(beer.description)
    }
}
