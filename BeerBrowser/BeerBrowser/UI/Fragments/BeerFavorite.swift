//
//  BeerFavorite.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 12.11.23.
//

import SwiftUI

struct BeerFavorite:View {
    var beer:Beer
    @EnvironmentObject var viewState:ViewState
    
    var body: some View {
        
        Button {
            viewState.roothandler(
                viewState.favorites.contains(beer)
                    ? .beer(.favorite(.remove(beer)))
                    : .beer(.favorite(.add(beer)))
            )
        } label: {
            if viewState.favorites.contains(beer) {
                Image(systemName: "star.fill")
            } else {
                Image(systemName: "star")
            }
        }
    }
}
