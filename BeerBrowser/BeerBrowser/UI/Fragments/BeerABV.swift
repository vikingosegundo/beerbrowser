//
//  BeerABV.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 12.11.23.
//

import SwiftUI

struct BeerABV:View {
    var beer:Beer
    
    var body: some View {
        Text("\(beer.abv, specifier: "%.1f")%")
    }
}
