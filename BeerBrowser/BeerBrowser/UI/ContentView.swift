//
//  ContentView.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject
           private var viewState   : ViewState
    @State private var selectedIndex = 0

    init(viewState vs:ViewState) {
        viewState = vs
    }

    var body: some View {
        NavigationSplitView {
            Picker("Favorite Color", selection: $selectedIndex) {
                Text("All Beers"   ).tag(0)
                Text("My Favorites").tag(1)
            }
            .pickerStyle(SegmentedPickerStyle())
            
            switch (selectedIndex, viewState.favorites.count) {
            case (0, _): List(viewState.allBeers) { beer in
                NavigationLink {
                    BeerDetail(beer: beer)
                }
            label: {
                BeerCell(beer: beer)
                }
            }
            .navigationTitle("Beers")
            case (1,0): List {
                Text("no favorites found")
            }
            .navigationTitle("Favorites")
            default:
                List(viewState.favorites) { beer in
                    NavigationLink {
                        BeerDetail(beer: beer)
                    }
                label: {
                    BeerCell(beer: beer)
                    }
                   
                }.navigationTitle("Favorites")
            }
        } detail: {
            Text("select a beer")
        }.environmentObject(viewState)
    }
}
