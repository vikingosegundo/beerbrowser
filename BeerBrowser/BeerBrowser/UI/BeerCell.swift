//
//  BeerCell.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 12.11.23.
//

import SwiftUI

struct BeerCell: View {
    var beer:Beer
    @EnvironmentObject var viewState:ViewState
    
    var body: some View {
        HStack {
            VStack {
                AsyncImage(url: beer.imageURL) {
                    $0
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 80, maxHeight: 120)
                } placeholder: {
                    Image("beerbottle")
                        .resizable()
                        .frame(width: 80, height: 120)
                }
            }.padding(10)
            VStack {
                HStack {
                    BeerName(beer:beer)
                    Spacer()
                    BeerFavorite(beer: beer)
                }
                HStack {
                    Spacer()
                    BeerABV(beer: beer)
                }
            }
        }.swipeActions {
            if viewState.favorites.contains(beer){
                Button {
                    viewState.roothandler(.beer(.favorite(.remove(beer))))
                } label: {
                    Text("remove from favorites")
                }
                .tint(.red)
            } else  {
                Button {
                    viewState.roothandler(.beer(.favorite(.add(beer))))
                } label: {
                    Text("add to favorites")
                }
                .tint(.green)
            }
        }
    }
}
