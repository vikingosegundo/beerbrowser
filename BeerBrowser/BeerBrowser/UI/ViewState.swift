//
//  ViewState.swift
//  BeerBrowser
//
//  Created by vikingosegundo on 09.11.23.
//

import SwiftUI

final class ViewState: ObservableObject {
    @Published public var allBeers   : [Beer] = []
    @Published public var favorites  : [Beer] = []
    @Published public var roothandler: (Message) -> ()

    public init(store:AppStore, roothandler: @escaping (Message) -> ()) {
        self.roothandler = roothandler
        store.updated { self.process(store.state()) }
        process(store.state())
    }

    public func process(_ appState: AppState) {
        DispatchQueue.main.async {
            self.allBeers = appState.beers
            self.favorites = appState.favorites
        }
    }
}
